USE `galleryshare`;
/*
User
Contrasena: MiPassWord
*/
INSERT INTO user (uuid, alias, name, email, pass, country, birthdate,  hiredate, isadmin)
    VALUES
    ('5fe87c6a1b929', 'Admin', 'Admin User', 'info.galleryshare@gmail.com', '447f15ad4b8b0fbb6a9fa6e236f803c2d128d1e2a99025fb70042e738d2dde61', 'Spain', '1992-02-28', NOW(), 1);

/*
Post
*/
INSERT INTO post (uuid, publisherid, supervisorid, title, content, imagesrc, hiredate, denied_cmt, tags)
    VALUES
    ('5fe881f7bd3b6', '5fe87c6a1b929', '5fe87c6a1b929', 'Como desenfocar el fondo sin Iphone', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. In tempor efficitur enim vitae euismod. Donec porttitor fermentum ex vitae pellentesque. Sed at orci vitae tellus fermentum tristique. Nulla aliquam augue quis magna consequat accumsan vitae eu purus. Donec interdum venenatis commodo. Aliquam erat volutpat. Proin in tortor felis. Morbi et maximus velit. Phasellus id turpis vitae ligula cursus malesuada vitae sed diam. Pellentesque cursus tellus est, et volutpat diam aliquam eget. Donec commodo nec felis eget facilisis. Nam volutpat, ex ut lobortis rutrum, nisi dolor finibus dolor, eget mattis justo dolor a eros. Morbi nisl lacus, interdum vel consectetur ac, luctus ut justo. Mauris ut elementum metus. Phasellus metus risus, suscipit eu ipsum nec, blandit consectetur tellus.<br>Vivamus bibendum ipsum id justo dapibus sagittis. Vivamus quis mauris euismod risus aliquam dignissim. Morbi lacinia auctor tincidunt. Etiam nec odio in justo lobortis lobortis at ut mauris. Duis ac nibh aliquam, efficitur justo sed, commodo nisl. Nullam vel magna sit amet enim sagittis luctus. In quam risus, sagittis in laoreet in, bibendum convallis leo. Maecenas eget lorem metus. Sed libero odio, mollis at purus quis, sodales luctus risus. Maecenas porta iaculis maximus. Aenean elit dui, maximus nec eleifend a, luctus volutpat leo.<br>Aenean sollicitudin, justo nec dapibus sollicitudin, ante urna porttitor nulla, eu aliquam dui orci venenatis ipsum. Vestibulum quis iaculis dolor, in ornare justo. In augue felis, pretium nec rhoncus nec, laoreet non lacus. Donec leo massa, fermentum eu eleifend ac, tempor a est. Sed et nunc ligula. Phasellus placerat, lacus eget sodales porttitor, lorem quam molestie augue, et laoreet quam purus eu sapien. Quisque est dolor, accumsan sit amet sem sit amet, lacinia bibendum mi. Phasellus lectus metus, fringilla in interdum at, dapibus gravida orci. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Donec eleifend, leo nec vestibulum malesuada, purus magna rhoncus lorem, id sollicitudin diam augue ac justo. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Nulla ac lobortis libero. Maecenas in purus ante. Morbi sodales nulla nec malesuada tincidunt.<br>Donec consectetur, turpis id ultrices ultricies, velit ante vehicula libero, in porta ex lacus at sem. Etiam laoreet dui nulla, nec vestibulum diam elementum quis. Donec et leo sodales dolor lacinia ornare eu at enim. Mauris mollis, tortor in ornare lacinia, augue arcu ultricies enim, a accumsan lorem lorem quis dui. Aliquam imperdiet, eros dignissim fermentum interdum, risus arcu pharetra metus, vel hendrerit enim erat ut lorem. Donec sit amet consectetur enim. Praesent in felis in dolor venenatis luctus in sed arcu. Donec scelerisque faucibus ex. Donec ac egestas elit, ut ornare ex. Morbi tempor, velit sit amet gravida fringilla, odio dolor pharetra neque, in bibendum libero mi vel lorem. Vivamus ut arcu sed sem euismod pellentesque. Aliquam sed odio non purus posuere fringilla. Nam porttitor lectus quam, vel cursus sem pellentesque a. Proin lacinia suscipit erat, sit amet ullamcorper felis egestas a. Donec interdum eget ligula ac porttitor.','assets/images/60212b97ec46d.png', '2020-12-25 22:30:04',null, "tag1,tag2,tag3,tag4,tag5");


/*
Comments
*/
INSERT INTO comment (uuid, userid, postid, content, hiredate)
    VALUES
    ('5fea2f343089f', '5fe87c6a1b929', '5fe881f7bd3b6', 'Fantástico', '2020-12-28 22:30:04');

/*
to_Rate
*/
INSERT INTO to_rate (userid, postid, rate)
    VALUES
    ('5fe87c6a1b929', '5fe881f7bd3b6', 5);





