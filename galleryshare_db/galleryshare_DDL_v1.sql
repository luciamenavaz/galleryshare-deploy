CREATE DATABASE  IF NOT EXISTS `galleryshare`
  DEFAULT CHARACTER SET utf8 
  DEFAULT COLLATE utf8_general_ci /*!80016 DEFAULT ENCRYPTION='N' */;
USE `galleryshare`;
-- MySQL dump 10.13  Distrib 8.0.20, for Win64 (x86_64)
--
-- Host: localhost    Database: galleryshare
-- ------------------------------------------------------
-- Server version	8.0.20

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `comment`
--

DROP TABLE IF EXISTS `comment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8 */;
CREATE TABLE `comment` (
  `uuid` varchar(13) NOT NULL,
  `userid` varchar(13) NOT NULL,
  `postid` varchar(13) NOT NULL,
  `content` varchar(200) NOT NULL,
  `hiredate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`uuid`),
  KEY `userid_comment_user_FK_idx` (`userid`),
  KEY `postid_comment_post_FK_idx` (`postid`),
  CONSTRAINT `postid_comment_post_FK` FOREIGN KEY (`postid`) REFERENCES `post` (`uuid`),
  CONSTRAINT `userid_comment_user_FK` FOREIGN KEY (`userid`) REFERENCES `user` (`uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `image`
--

DROP TABLE IF EXISTS `image`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8 */;
CREATE TABLE `image` (
  `uuid` varchar(13) NOT NULL,
  `userid` varchar(13) NOT NULL,
  `title` varchar(60) NOT NULL,
  `footer` varchar(100) DEFAULT NULL,
  `location` varchar(150) DEFAULT NULL,
  `imagesrc` varchar(300) NOT NULL,
  `tags` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`uuid`),
  KEY `userid_image_user_FK_idx` (`userid`),
  CONSTRAINT `userid_image_user_FK` FOREIGN KEY (`userid`) REFERENCES `user` (`uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `notification`
--

DROP TABLE IF EXISTS `notification`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8 */;
CREATE TABLE `notification` (
  `uuid` varchar(13) NOT NULL,
  `userid` varchar(13) NOT NULL,
  `element_type` varchar(10) NOT NULL,
  `element_id` varchar(13) NOT NULL,
  `action` tinyint(1) NOT NULL,
  PRIMARY KEY (`uuid`),
  KEY `userid_notification_user_FK_idx` (`userid`),
  CONSTRAINT `userid_notification_user_FK` FOREIGN KEY (`userid`) REFERENCES `user` (`uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `post`
--

DROP TABLE IF EXISTS `post`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8 */;
CREATE TABLE `post` (
  `uuid` varchar(13) NOT NULL,
  `publisherid` varchar(13) NOT NULL,
  `supervisorid` varchar(13) NULL, /*Editado por que lo post pueden no estar revisados*/
  `title` varchar(200) NOT NULL,
  `content` text NOT NULL,
  `imagesrc` varchar(300) DEFAULT NULL,
  `hiredate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `denied_cmt` varchar(200) DEFAULT NULL,
  `tags` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`uuid`),
  KEY `publisherid_post_user_FK_idx` (`publisherid`),
  KEY `supervisorid_post_user_FK_idx` (`supervisorid`),
  CONSTRAINT `publisherid_post_user_FK` FOREIGN KEY (`publisherid`) REFERENCES `user` (`uuid`),
  CONSTRAINT `supervisorid_post_user_FK` FOREIGN KEY (`supervisorid`) REFERENCES `user` (`uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `to_rate`
--

DROP TABLE IF EXISTS `to_rate`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8 */;
CREATE TABLE `to_rate` (
  `userid` varchar(13) NOT NULL,
  `postid` varchar(13) NOT NULL,
  `rate` tinyint(1) NOT NULL,
  PRIMARY KEY (`userid`,`postid`),
  KEY `postid_torate_post_FK_idx` (`postid`),
  KEY `userid_torate_user_FK_idx` (`userid`),
  CONSTRAINT `postid_torate_post_FK` FOREIGN KEY (`postid`) REFERENCES `post` (`uuid`),
  CONSTRAINT `userid_torate_user_FK` FOREIGN KEY (`userid`) REFERENCES `user` (`uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `uuid` varchar(13) NOT NULL,
  `alias` varchar(15) NOT NULL,
  `name` varchar(60) NOT NULL,
  `email` varchar(254) NOT NULL,
  `pass` longblob NOT NULL,
  `country` varchar(50) NOT NULL, 
  `birthdate` timestamp NOT NULL,
  `hiredate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `isadmin` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-12-22 19:00:54
