<?php

/**
 * CONSTANT ROUTES DEFINITION
 * THE ROUTES MUST BE RELATIVES TO ROOT FOLDER
 */

define('CTRLPATH', 'src/controllers/');
define('MODELPATH', 'src/models/');
define('ENTITYPATH', 'src/entities/');
define('HELPERPATH', 'src/helpers/');

define('DB_CONFIG', [
    'db_host' => 'localhost',
    'db_user' => 'galleryshare',
    'db_pass' => 'galleryshare',
    'db_name' => 'galleryshare'
]);
