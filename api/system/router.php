<?php

/**
 * ARRAY OF CONTROLLERS ASSIGNED MANUAL
 * IF METHOD IS NOT DEFINED OR EMPTY, IT TAKES THE DEFAULT METHOD
 * THE CONTROLLER ROUTES ARE RELATIVE TO THE CONTROLLERS DIRECTORY
 */
$routes = [
    [
        'uri'            => '/user/login',
        'controllerPath' => 'users/Users',
        'method'         => 'loginUser',
        'httpMethod'     => 'POST'
    ],
    [
        'uri'            => '/user/logout',
        'controllerPath' => 'users/Users',
        'method'         => 'logoutUser',
        'httpMethod'     => 'GET'
    ],
    [
        'uri'            => '/user',
        'controllerPath' => 'users/Users',
        'method'         => 'createUser',
        'httpMethod'     => 'POST'
    ],
    [
        'uri'            => '/user',
        'controllerPath' => 'users/Users',
        'method'         => 'deleteUser',
        'httpMethod'     => 'DELETE'
    ],
    [
        'uri'            => '/user/allusers',
        'controllerPath' => 'users/Users',
        'method'         => 'getAllUsers',
        'httpMethod'     => 'GET'
    ],
    [
        'uri'            => '/user/pass',
        'controllerPath' => 'users/Users',
        'method'         => 'updateUserPass',
        'httpMethod'     => 'PUT'
    ],
    [
        'uri'            => '/user',
        'controllerPath' => 'users/Users',
        'method'         => 'updateUser',
        'httpMethod'     => 'PUT'
    ],
    [
        'uri'            => '/post',
        'controllerPath' => 'posts/Posts',
        'method'         => 'createPost',
        'httpMethod'     => 'POST'
    ],
    [
        'uri'            => '/allpublicpost',
        'controllerPath' => 'posts/Posts',
        'method'         => 'getAllPublicPosts',
        'httpMethod'     => 'GET'
    ],
    [
        'uri'            => '/userposts',
        'controllerPath' => 'posts/Posts',
        'method'         => 'getUserPosts',
        'httpMethod'     => 'GET'
    ],
    [
        'uri'            => '/allpost',
        'controllerPath' => 'posts/Posts',
        'method'         => 'getAllUsersPosts',
        'httpMethod'     => 'GET'
    ],
    [
        'uri'            => '/post',
        'controllerPath' => 'posts/Posts',
        'method'         => 'delete',
        'httpMethod'     => 'DELETE'
    ],
    [
        'uri'            => '/post',
        'controllerPath' => 'posts/Posts',
        'method'         => 'update',
        'httpMethod'     => 'PUT'
    ],
    [
        'uri'            => '/post',
        'controllerPath' => 'posts/Posts',
        'method'         => 'getPost',
        'httpMethod'     => 'GET'
    ],
    [
        'uri'            => '/approve',
        'controllerPath' => 'posts/Posts',
        'method'         => 'approvePost',
        'httpMethod'     => 'GET'
    ],
    [
        'uri'            => '/reject',
        'controllerPath' => 'posts/Posts',
        'method'         => 'rejectPost',
        'httpMethod'     => 'POST'
    ],
    [
        'uri'            => '/setpending',
        'controllerPath' => 'posts/Posts',
        'method'         => 'setPending',
        'httpMethod'     => 'GET'
    ],
    [
        'uri'            => '/comment',
        'controllerPath' => 'comments/Comments',
        'method'         => 'createComment',
        'httpMethod'     => 'POST'
    ],
    [
        'uri'            => '/comment',
        'controllerPath' => 'comments/Comments',
        'method'         => 'deleteComment',
        'httpMethod'     => 'DELETE'
    ],
    [
        'uri'            => '/allimages',
        'controllerPath' => 'posts/Posts',
        'method'         => 'getAllImages',
        'httpMethod'     => 'GET'
    ],
    [
        'uri'            => '/image',
        'controllerPath' => 'posts/Posts',
        'method'         => 'getImage',
        'httpMethod'     => 'GET'
    ],
    [
        'uri'            => '/rate',
        'controllerPath' => 'rates/Rates',
        'method'         => 'setRate',
        'httpMethod'     => 'POST'
    ],
    [
        'uri'            => '/send',
        'controllerPath' => 'EmailController',
        'method'         => 'sendEmail',
        'httpMethod'     => 'POST'
    ]


];
