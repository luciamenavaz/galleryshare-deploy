<?php

class CoreModel
{
    protected PDO $connection;

    public function __construct()
    {

        $this->connection = new PDO(
            "mysql:host=" . DB_CONFIG['db_host'] . ";dbname=" . DB_CONFIG['db_name'],
            DB_CONFIG['db_user'],
            DB_CONFIG['db_pass'],
            [PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"]
        );
        $this->connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    }

    /**
     * This function returns an array of rows collected from the database
     * @param string $query The query to execute
     * @param array|null $data Array of elements for bind in the prepared statement
     * @return array An array of rows returned by de select
     */
    public function getArrayRows(string $query, array $data = null): array
    {
        $stmt = $this->connection->prepare($query);

        $stmt->execute($data);

        $rows = [];

        while ($row = $stmt->fetch()) {
            $rows[] = $row;
        }

        return $rows;
    }

    /**
     * This function returns an array of attributes collected from first row the database
     * @param string $query The query
     * @param array|null $data Array of elements for bind in the prepared statement
     * @return array An array of attributes of the first row
     */
    public function getRow(string $query, array $data = null): array
    {
        $stmt = $this->connection->prepare($query);

        $stmt->execute($data);

        $result = $stmt->fetch();

        if (!$result) {
            return [];
        }

        return $result;
    }

    /**
     * This function returns the number of affected rows
     * @param string $query The query
     * @param array|null $data Array of elements for bind in the prepared statement
     * @return int The affected rows of the query
     */
    public function execQuery(string $query, array $data = null): int
    {
        $stmt = $this->connection->prepare($query);

        if ($stmt->execute($data)) {

            return $stmt->rowCount();
        }
    }
}
