<?php


class URIParser
{
    private array $routes = [];
    private string $uri;
    private string $httpMethod;

    public function __construct(array $routes, string $uri, string $httpMethod)
    {
        $this->routes     = $routes;
        $this->uri        = $uri;
        $this->httpMethod = $httpMethod;

        $this->findController();
    }

    /**
     * INSTANCE THE CONTROLLER AND CALL THE EXECT FUNCTION
     */
    private function findController()
    {
        if (($controller = $this->checkRouter()) === null) {
            http_response_code(404);
            die('Controller not found');
        }

        $this->execController($controller);
    }

    /**
     * CHEK IF THE URI EXISTS IN ROUTER ARRAY
     * AND RETURN THE CONTROLLER
     *
     * IF URI CONTAINS INJECTION CODE DON'T MATCH WITH THE ROUTER URI
     * AND THE EXECUTION FINISH WITH ERROR CODE 404
     */
    private function checkRouter()
    {
        foreach ($this->routes as $controller) {
            if ($controller['uri'] === $this->uri && $controller['httpMethod'] === $this->httpMethod) {
                return $controller;
            }
        }
        return null;
    }

    /**
     * CHEK IF THE PARSED CONTROLLER EXITS
     * IF EXISTS EXECUTE THE METHOD
     * @param array of the parsed controller have de router structure
     */
    private function execController(array $controller)
    {
        $controllerAbsolutePath = CTRLPATH . $controller['controllerPath'] . '.php';
        $URIsegments            = explode('/', $controller['controllerPath']);

        //Obtain the class name
        if ($URIsegments === false) {
            $controllerClass = $controller['controllerPath'];
        } else {
            $controllerClass = $URIsegments[count($URIsegments) - 1];
        }

        if (!file_exists($controllerAbsolutePath)) {
            http_response_code(404);
            die('Error, the controller has not been find');
        }

        require_once($controllerAbsolutePath);

        if (!method_exists($controllerClass, $controller['method'])) {
            http_response_code(404);
            die('Error method no found in controllers ' . $controllerAbsolutePath);
        }

        try {
            call_user_func([new $controllerClass, $controller['method']]);
        } catch (Exception $exception) {
            http_response_code(500);
            die($exception->getMessage());
        }

    }

}
