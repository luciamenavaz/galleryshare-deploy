<?php

declare(strict_types=1);


class Notification
{

    /** @var [object Object] */
    private Uuid $uuid;

    /** @var [object Object] */
    private User $user;

    /** @var INotification */
    private INotification $element;

    /**
     * @var int type of the notification, it can be of three types:
     * 1 = admin will aprove
     * 2 = it has been approved
     * 3=read new comment
     */
    private int $action;

    /**
     * @param [object Object] $uuid 
     * @param [object Object] $user 
     * @param [object Object] $element 
     * @param int $action
     */
    public function __construct([object Object] $uuid, [object Object] $user, [object Object] $element, int $action)
    {
        // TODO implement here
    }

    /**
     * @return array
     */
    public function getPublicData(): array
    {
        // TODO implement here
        return [];
    }

}
