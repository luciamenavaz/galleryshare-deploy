<?php

declare(strict_types=1);

class User
{

    /** @var string */
    private string $userAlias;

    /** @var string */
    private string $name;

    /** @var string */
    private string $email;

    /** @var string */
    private string $pass;

    /** @var Uuid */
    private Uuid $uuid;

    /** @var string */
    private string $country;

    /** @var DateTimeImmutable */
    private DateTimeImmutable $birthDate;

    /** @var DateTimeImmutable */
    private DateTimeImmutable $hireDate;

    /** @var bool */
    private bool $isAdmin;

    /**
     * User constructor.
     * @param string $userAlias
     * @param string $name
     * @param string $email
     * @param string $pass
     * @param Uuid $uuid
     * @param string $country
     * @param DateTimeImmutable $birthDate
     * @param DateTimeImmutable $hireDate
     * @param bool $isAdmin
     */
    public function __construct(
        string $userAlias,
        string $name,
        string $email,
        string $pass,
        Uuid $uuid,
        string $country,
        DateTimeImmutable $birthDate,
        DateTimeImmutable $hireDate,
        bool $isAdmin = false
    ) {

        if (!$this->isAlias($userAlias)) {
            throw new Exception("The alias can only contain uppercase letters, lowercase letters and numbers.");
        }

        if (!$this->isName($name)) {
            throw new Exception("The name can only contain uppercase letters and lowercase letters.");
        }

        if (!$this->isMail($email)) {
            throw new Exception("The email must be in valid format.");
        }

        if (!$this->isPass($pass)) {
            throw new Exception("The password can only contain uppercase and lowercase letters and numbers and must be a minimum of 8 characters.");
        }

        if (!$this->isCountry($country)) {
            throw new Exception("The country must exist.");
        }

        if (!$this->isDate($birthDate)) {
            throw new Exception("The user must be over 14 years old.");
        }


        $this->userAlias = $userAlias;
        $this->name      = $name;
        $this->email     = $email;
        $this->pass      = $pass;
        $this->uuid      = $uuid;
        $this->country   = $country;
        $this->birthDate = $birthDate;
        $this->hireDate  = $hireDate;
        $this->isAdmin   = $isAdmin;
    }

    /**
     * @param string $userAlias
     * @param string $name
     * @param string $email
     * @param string $pass
     * @param uuid $uuid
     * @param string $country
     * @param DateTimeImmutable $birthDate
     * @param DateTimeImmutable $hireDate
     * @param bool $isAdmin
     */


    /**
     * @param string $attName
     */
    public function __get(string $attName)
    {
        switch ($attName) {
            case 'UserAlias':
                return $this->userAlias;
            case 'Name':
                return $this->name;
            case 'Email':
                return $this->email;
            case 'Pass':
                return $this->pass;
            case 'Uuid':
                return $this->uuid;
            case 'Country':
                return $this->country;
            case 'BirthDate':
                return $this->birthDate;
            case 'HireDate':
                return $this->hireDate;
            case 'IsAdmin':
                return $this->isAdmin;
        }
    }

    public function getPublicData(): array
    {
        return [
            'username'  => $this->name,
            'alias'     => $this->userAlias,
            'email'     => $this->email,
            'uuid'      => $this->uuid->__toString(),
            'country'   => $this->country,
            'birthDate' => $this->birthDate->getTimestamp(),
            'hireDate'  => $this->hireDate->getTimestamp(),
            'isAdmin'   => $this->isAdmin
        ];
    }

    /**
     * @param string $value
     */
    public function setUserAlias(string $value)
    {
        if (!$this->isAlias($value)) {
            throw new Exception("The alias can only contain uppercase letters, lowercase letters and numbers.");
        }

        $this->userAlias = $value;
    }

    /**
     * @param string $value
     */
    public function setName(string $value)
    {
        if (!$this->isName($value)) {
            throw new Exception("The name can only contain uppercase letters and lowercase letters.");
        }

        $this->name = $value;
    }

    /**
     * @param string $value
     */
    public function setEmail(string $value)
    {
        if (!$this->isMail($value)) {
            throw new Exception("The email must be in valid format.");
        }

        $this->email = $value;
    }

    /**
     * @param string $value
     */
    public function setPass(string $value)
    {
        if (!$this->isPass($value)) {
            throw new Exception("The password can only contain uppercase and lowercase letters and numbers and must be a minimum of 8 characters.");
        }

        $this->pass = $value;
    }

    /**
     * @param string $value
     */
    public function setCountry(string $value)
    {
        if (!$this->isCountry($value)) {
            throw new Exception("The country must exist.");
        }

        $this->country = $value;
    }

    /**
     * @param DateTimeImmutable $value
     */
    public function setBirthDate(DateTimeImmutable $value)
    {
        if (!$this->isDate($value)) {
            throw new Exception("The user must be over 14 years old.");
        }

        $this->birthDate = $value;
    }

    /**
     * @param bool $admin
     */
    public function setAdmin(bool $admin)
    {
        $this->isAdmin = $admin;
    }

    /*-----------------------Validations------------------------*/

    /**
     * @param string $alias
     * @return bool
     */
    private function isAlias(string $alias): bool
    {
        $pattern = "/^[a-zA-Z0-9_-]*$/";

        if (preg_match($pattern, trim($alias))) {
            return true;
        }

        return false;
    }

    /**
     * @param string $name
     * @return bool
     */
    private function isName(string $name): bool
    {
        $pattern = "/^[a-zA-ZÀ-ÿñÑ(\s?)]{1,60}$/";

        if (preg_match($pattern, trim($name))) {
            return true;
        }

        return false;
    }

    /**
     * @param string $mail
     * @return bool
     */
    private function isMail(string $mail): bool
    {
        $pattern = "/^[a-zA-Z0-9\._-]+@[a-zA-Z]+\.[a-zA-Z]{2,}$/";

        if (preg_match($pattern, trim($mail))) {
            return true;
        }

        return false;
    }

    /**
     * @param string $pass
     * @return bool
     */
    private function isPass(string $pass): bool
    {
        $pattern = "/^[^\"\']{6,}$/";

        if (preg_match($pattern, trim($pass))) {
            return true;
        }

        return false;
    }

    /**
     * @param string $country
     * @return bool
     */
    private function isCountry(string $country): bool
    {
        $jsonAllCountries = file_get_contents("./src/assets/allCountries.json");
        if ($jsonAllCountries == false) {
            return false;
        }

        $countries = json_decode($jsonAllCountries, true);
        foreach ($countries as $key) {
            if (in_array($country, $key)) {
                return true;
            }
        }
        return false;
    }

    /**
     * @param DateTimeImmutable $birthDate
     * @return bool
     */
    private function isDate(DateTimeImmutable $birthDate): bool
    {
        $now = new DateTime();
        $interval = $now->diff($birthDate);


        if ($interval->y >= 14) {
            return true;
        }

        return false;
    }
}
