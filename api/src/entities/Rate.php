<?php


class Rate
{
    private User $user;
    private Post $post;
    private int $value;

    /**
     * Rate constructor.
     * @param User $user
     * @param Post $post
     * @param int $value
     * @throws Exception
     */
    public function __construct(User $user, Post $post, int $value)
    {
        if (!$this->isValue($value)) {
            throw new Exception("The value $value is not allowed in a rate");
        }
        $this->user = $user;
        $this->post = $post;
        $this->value = $value;
    }

    /**
     * @return array
     */
    public function getPublicData(): array
    {
        return [
            'value' => $this->value
        ];
    }

    /**
     * @return int
     */
    public function getValue(): int
    {
        return $this->value;
    }
    public function __get(string $attName)
    {
        switch ($attName) {
            case 'User':
                return $this->user;
            case 'Post':
                return $this->post;
            case 'Value':
                return $this->value;
        }
    }

    private function isValue(int $value): bool
    {
        if ($value < 0 || $value > 5) {
            return false;
        }
        return true;
    }
}
