<?php

declare(strict_types=1);


class Valuation
{

    /** @var int */
    private int $rate;

    /** @var User */
    private User $user;

    /**
     * @param int $rate 
     * @param User $user
     */
    public function __construct(int $rate, User $user)
    {
        $this->user = $user;
        $this->rate = $rate;
    }

    /**
     * @return int
     */
    public function getRate(): int
    {
        return $this->rate;
    }

    /**
     * @return User
     */
    public function getUser(): User
    {
        return $this->user;
    }
}
