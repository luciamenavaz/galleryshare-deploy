<?php

declare(strict_types=1);


class Comment
{

    /** @var Uuid */
    private Uuid $uuid;

    /** @var User */
    private User $user;

    /** @var Uuid */
    private Uuid $post;

    /** @var string */
    private string $content;

    /** @var DateTimeImmutable */
    private DateTimeImmutable $hireDate;

    /**
     * @param Uuid $uuid
     * @param User $user
     * @param Uuid $post
     * @param string $content
     * @param DateTimeImmutable|null $hireDate
     * @throws Exception
     */
    public function __construct(Uuid $uuid, User $user, Uuid $post, string $content, DateTimeImmutable $hireDate)
    {
        if (!$this->isContent($content)) {
            throw new Exception('The comment content is not allowed');
        }
        $this->uuid = $uuid;
        $this->user = $user;
        $this->post = $post;
        $this->content = $content;
        $this->hireDate = $hireDate;

    }

    /**
     * @param string $newContent
     */
    public function setContent(string $newContent)
    {
        if ($this->isContent($newContent)) {
            $this->content = $newContent;
        }
    }

    /**
     * @param string $attName
     */
    public function __get(string $attName)
    {
        switch ($attName) {
            case 'Uuid':
                return $this->uuid;
            case 'User':
                return $this->user;
            case 'Post':
                return $this->post;
            case 'Content':
                return $this->content;
            case 'HireDate':
                return $this->hireDate;

        }
    }

    /**
     * @param string $content
     * @return bool
     */
    private function isContent(string $content): bool
    {

        if (strlen($content) < 1 || preg_match("/^\s+$/", $content) || !preg_match('/^.{1,200}$/', $content)) {
            return false;
        }
        return true;
    }

    /**
     * @return array
     */
    public function getPublicData(): array
    {
        return [
            'uuid' => $this->uuid->__ToString(),
            'user' => $this->user->getPublicData(),
            'content' => $this->content,
            'hireDate' => $this->hireDate->getTimestamp()
        ];
    }

}
