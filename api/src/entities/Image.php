<?php

declare(strict_types=1);


class Image
{

    /** @var [object Object] */
    private Uuid $uuid;

    /** @var [object Object] */
    private User $user;

    /** @var string */
    private string $title;

    /** @var string */
    private string $footer;

    /** @var string */
    private string $location;

    /** @var string */
    private string $image;

    /** @var array */
    private array $tags;

    /**
     * @param [object Object] $uuid 
     * @param [object Object] $user 
     * @param string $title 
     * @param string $image 
     * @param string $footer 
     * @param string $location 
     * @param array $tags
     */
    public function __construct([object Object] $uuid, [object Object] $user, string $title, string $image, string $footer = null, string $location = null, array $tags = null)
    {
        // TODO implement here
    }

    /**
     * @return array
     */
    public function getPublicData(): array
    {
        // TODO implement here
        return [];
    }

    /**
     * @param string $newTitle
     */
    public function setTitle(string $newTitle)
    {
        // TODO implement here
    }

    /**
     * @param string $newFooter
     */
    public function setFooter(string $newFooter)
    {
        // TODO implement here
    }

    /**
     * @param string $newLocation
     */
    public function setLocation(string $newLocation)
    {
        // TODO implement here
    }

    /**
     * @param array $tags
     */
    public function setTags(array $tags)
    {
        // TODO implement here
    }

    /**
     * @param string $title 
     * @return bool
     */
    private function isTitle(string $title): bool
    {
        // TODO implement here
        return false;
    }

    /**
     * @param string $content 
     * @return bool
     */
    private function isContent(string $content): bool
    {
        // TODO implement here
        return false;
    }

    /**
     * @param string $location
     */
    private function isLocation(string $location)
    {
        // TODO implement here
    }

}
