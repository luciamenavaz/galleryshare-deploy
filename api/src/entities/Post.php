<?php

class Post
{

    /** @var Uuid */
    private Uuid $uuid;

    /** @var string */
    private string $title;

    /** @var string */
    private string $content;

    /** @var string */
    private string $image;

    /** @var User */
    private User $publisher;

    /** @var DateTimeImmutable */
    private DateTimeImmutable $hireDate;

    /** @var User | null */
    private ?User $supervisor;

    /** @var array */
    private array $comments;

    /** @var array */
    private array $rate;

    /** @var array */
    private array $tags;

    /** @var string | null */
    private ?string $denied_cmt;

    /**
     * @param Uuid $uuid
     * @param string $title
     * @param string $content
     * @param User $publisher
     * @param string $image
     * @param DateTImeImmutable|null $hireDate
     * @param array $rate
     * @param array $comments
     * @param array $tags
     * @param User|null $supervisor
     * @throws Exception
     */
    public function __construct(
        Uuid $uuid,
        string $title,
        string $content,
        User $publisher,
        string $image,
        DateTimeImmutable $hireDate,
        array $rate,
        array $comments,
        array $tags,
        User $supervisor = null
    ) {
        if (!$this->isTitle($title)) {
            throw new Exception('Title not allowed');
        }

        if (!$this->isContent($content)) {
            throw new Exception('Content is empty');
        }

        if (!$this->isImage($image)) {
            throw new Exception('Only images base64 allowed');
        }

        if ($supervisor != null && !$this->isSupervisor($supervisor)) {
            throw new Exception('The supervisor is not admin');
        }

        $this->uuid       = $uuid;
        $this->title      = $title;
        $this->content    = $content;
        $this->publisher  = $publisher;
        $this->image      = $image;
        $this->rate       = $rate;
        $this->comments   = $comments;
        $this->supervisor = $supervisor;
        $this->tags       = $tags;
        $this->hireDate   = $hireDate;

        $this->denied_cmt = null;

    }

    /**
     * @param string $attName
     */
    public function __get(string $attName)
    {
        switch ($attName) {
            case 'Uuid':
                return $this->uuid;
            case 'Title':
                return $this->title;
            case 'Content':
                return $this->content;
            case 'Publisher':
                return $this->publisher;
            case 'Image':
                return $this->image;
            case 'Supervisor':
                return $this->supervisor;
            case 'Comments':
                return $this->comments;
            case 'Rate':
                return $this->rate;
            case 'Tags':
                return $this->tags;
            case 'Denied_cmt':
                return $this->denied_cmt;
            case 'HireDate':
                return $this->hireDate;
        }

    }

    /**
     * @param string $value
     * @throws Exception
     */
    public function setTitle(string $value)
    {
        if (!$this->isTitle($value)) {
            throw new Exception('Title not allowed');
        }
        $this->title = $value;
    }

    /**
     * @param string $text
     */
    public function setContent(string $text)
    {
        if (!$this->isContent($text)) {
            throw new Exception('Content not allowed');
        }
        $this->content = $text;
    }

    /**
     * @param string $value
     * @throws Exception
     */
    public function setImage(string $value)
    {
        if (!$this->isImage($value)) {
            throw new Exception('Image not allowed');
        }
        $this->image = $value;
    }

    /**
     * @param User|null $supervisor
     * @throws Exception
     */
    public function setSupervisor(?User $supervisor)
    {
        if ($supervisor !== null && !$this->isSupervisor($supervisor)) {
            throw new Exception('The supervisor not have admin privileges');
        }
        $this->supervisor = $supervisor;
    }

    public function setPending()
    {
        $this->supervisor = null;
        $this->denied_cmt = null;
    }

    public function setTags(array $tags)
    {
        $this->tags = $tags;
    }

    /**
     * @return int
     */
    public function getRateAverage(): int
    {
        if (!empty($this->rate)) {
            return $this->calcAverageRate();
        } else {
            return 0;
        }

    }

    /**
     * @param Rate $rate
     */
    public function setOneRate(Rate $rate)
    {
        $this->rate[] = $rate;
    }

    /**
     * @param string|null $cmt
     */
    public function setDenied(?string $cmt)
    {
        $this->denied_cmt = $cmt;
    }

    /**
     * @param User $user
     */
    public function setPublisher($user)
    {
        $this->publisher = $user;
    }

    /**
     * @param Comment $newComment
     */
    public function addComment(Comment $newComment)
    {
        $this->comments[] = $newComment;
    }

    /**
     * @param Uuid $uuidComment
     * @return Comment|null
     */
    public function getComment(Uuid $uuidComment): ?Comment
    {
        foreach ($this->comments as $comment) {
            if ($comment->Uuid == $uuidComment) {
                return $comment;
            }
        }
        return null;

    }

    public function getResumeContent(): string
    {
        $resumedContent = strip_tags($this->content);
        if (strlen($resumedContent) > 100) {
            $resumedContent = substr($resumedContent, 0, 110);
        }
        return $resumedContent;
    }

    /**
     * @param Uuid $uuidComment
     * @return bool
     */
    public function deleteComment(Uuid $uuidComment): bool
    {
        foreach ($this->comments as $key => $value) {
            if ($value->Uuid == $uuidComment) {
                $this->comments = array_splice($this->comments, $key, 1);
                return true;
            }
        }
        return false;
    }

    /**
     * @return array
     */
    public function getPublicData(): array
    {
        return [
            'uuid'      => $this->uuid->__ToString(),
            'publisher' => $this->publisher->getPublicData(),
            'title'     => $this->title,
            'content'   => $this->content,
            'image'     => $this->image,
            'hireDate'  => $this->hireDate->getTimestamp(),
            'tags'      => $this->tags,
            'rate'      => $this->getRateAverage()
        ];
    }

    public function isAproved(): bool
    {
        if ($this->supervisor != null && $this->denied_cmt === null) {
            return true;
        } else {
            return false;
        }
    }


    /*-----------------------Validations------------------------*/

    /**
     * @param User $supervisor
     * @return bool
     */
    private function isSupervisor(User $supervisor)
    {
        return $supervisor->IsAdmin;
    }

    /**
     * @param string $title
     * @return bool
     */
    private function isTitle(string $title): bool
    {
        if (!$this->isFully($title) || !preg_match('/.{1,200}$/', $title)) {
            return false;
        }
        return true;
    }

    /**
     * @param string $content
     * @return bool
     */
    private function isContent(string $content): bool
    {
        return $this->isFully($content);
    }

    /**
     * @param string $content
     * @return bool
     */
    private function isImage(string $content): bool
    {
        return $this->isFully($content);
    }

    /**
     * @param string $word
     * @return bool
     */
    private function isFully(string $word): bool
    {
        if (strlen($word) < 1 || preg_match("/^\s+$/", $word)) {
            return false;
        }
        return true;
    }

    /*-----------------------Aux Function------------------------*/

    private function calcAverageRate(): int
    {
        $acum = 0;

        foreach ($this->rate as $value) {
            $acum += $value['rate'];
        }
        return intval(round($acum / count($this->rate), 0, PHP_ROUND_HALF_UP));
    }
}
