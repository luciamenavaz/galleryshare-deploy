<?php

declare(strict_types=1);


interface INotification
{

    public  $Uuid;

    /**
     * 
     */
    public function getType();

}