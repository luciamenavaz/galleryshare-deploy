<?php

declare(strict_types=1);


/**
 * Se crea esta unidad para unificar la implementación en las clases
 */
class Uuid
{

    /** @var string */
    private string $value;

    /**
     * @param string|null $value
     */
    public function __construct(string $value = null)
    {
        if($value !== null){
            $this->value = $value;
        }else{
            $this->value = $this->generateNew();
        }
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return $this->value;
    }

    /**
     * @return string
     */
    private function generateNew(): string
    {
        return uniqid();
    }

}
