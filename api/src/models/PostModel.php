<?php
require_once(HELPERPATH . 'ImageHelper.php');

class PostModel extends CoreModel
{


    /**
     * PostModel constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    public function insertNewPost($newPost)
    {
        $query      = "INSERT INTO post (uuid, publisherid, supervisorid, title, content, imagesrc, hiredate, denied_cmt, tags) 
                    VALUES (:uuid, :publisherid, :supervisorid, :title, :content, :imagesrc, :hiredate, :denied_cmt, :tags);";
        $imageSrc   = ImageHelper::saveImage($newPost->Image);
        $formatHire = $newPost->HireDate->format("Y-m-d H:i:s");
        $tags       = implode(',', $newPost->Tags);

        if ($newPost->Supervisor !== null) {
            $supervisorId = $newPost->Supervisor->Uuid;
        } else {
            $supervisorId = null;
        }

        return parent::execQuery($query, [
            'uuid'         => $newPost->Uuid->__ToString(),
            'publisherid'  => $newPost->Publisher->Uuid->__ToString(),
            'supervisorid' => $supervisorId,
            'title'        => $newPost->Title,
            'content'      => $newPost->Content,
            'imagesrc'     => $imageSrc,
            'hiredate'     => $formatHire,
            'denied_cmt'   => null,
            'tags'         => $tags
        ]);
    }

    public function getUserPosts(Uuid $userUuid)
    {
        $query = "SELECT * FROM post WHERE publisherid = :publisherid";
        $posts = parent::getArrayRows($query, ['publisherid' => $userUuid->__ToString()]);

        $postsArray = [];
        foreach ($posts as $post) {
            $postsArray[] = $this->generatePost($post);
        }
        return $postsArray;
    }

    public function getAllPost(): array
    {
        $query = 'SELECT * FROM post ORDER BY hiredate DESC';
        $posts = parent::getArrayRows($query);

        $postsArray = [];
        foreach ($posts as $post) {
            $postsArray[] = $this->generatePost($post);
        }
        return $postsArray;
    }

    private function generatePost($dataArray): Post
    {
        $userModel = new UserModel();
        $publisher = $userModel->getUser($userModel->getUserRow(new Uuid($dataArray['publisherid'])));

        if ($dataArray['supervisorid'] != null) {
            $supervisor = $userModel->getUser($userModel->getUserRow(new Uuid($dataArray['supervisorid'])));
        } else {
            $supervisor = null;
        }

        $tags = explode(',', $dataArray['tags']);
        //TODO llamar a el modelo de los comentarios para recuperar un array de OBJETOS
        // comentrios después insertar en el new Post

        $commentModel = new CommentModel();
        $comments = $commentModel->getComments(new Uuid($dataArray['uuid']));

        //TODO llamar a el modelo de los rates para recuperar el array de OBJETOS valoraciones
        // e  insertar en el new Post

        $rateModel = new RateModel();
        $rate = $rateModel->getRates(new Uuid($dataArray['uuid']));

        $post = new Post(
            new Uuid($dataArray['uuid']),
            $dataArray['title'],
            $dataArray['content'],
            $publisher,
            $dataArray['imagesrc'],
            new DateTimeImmutable($dataArray['hiredate']),
            $rate,
            $comments,
            $tags,
            $supervisor
        );

        if ($dataArray['denied_cmt'] != null) {
            $post->setDenied($dataArray['denied_cmt']);
        }

        return $post;
    }

    public function getPost(Uuid $uuid)
    {
        $query = 'SELECT * FROM post WHERE uuid = :postid';
        $post  = parent::getRow($query, ['postid' => $uuid->__ToString()]);

        if (count($post) > 1) {
            return $this->generatePost($post);
        } else {
            throw new Exception('This post not exist');
        }
    }

    public function removePost(Uuid $uuid)
    {
        $sql = "DELETE FROM comment WHERE postid = :id";
        parent::execQuery($sql, [":id" => $uuid]);

        $sql = "DELETE FROM to_rate WHERE postid = :id";
        parent::execQuery($sql, [":id" => $uuid]);

        $query = 'DELETE FROM post WHERE uuid = :uuid';
        return parent::execQuery($query, [":uuid" => $uuid]);
    }

    public function updatePost($newPost)
    {
        $query      = "UPDATE post SET uuid = :uuid,
                                       publisherid = :publisherid,
                                       supervisorid = :supervisorid,
                                       title = :title,
                                       content = :content,
                                       imagesrc = :imagesrc,
                                       hiredate = :hiredate,
                                       denied_cmt = :denied_cmt,
                                       tags = :tags
                        WHERE uuid = :uuid;";
        $formatHire = $newPost->HireDate->format("Y-m-d H:i:s");
        $tags       = implode(',', $newPost->Tags);

        if ($newPost->Supervisor !== null) {
            $supervisorId = $newPost->Supervisor->Uuid;
        } else {
            $supervisorId = null;
        }

        if ($newPost->Denied_cmt !== null) {
            $denied_cmt = $newPost->Denied_cmt;
        } else {
            $denied_cmt = null;
        }

        return parent::execQuery($query, [
            'uuid'         => $newPost->Uuid->__ToString(),
            'publisherid'  => $newPost->Publisher->Uuid->__ToString(),
            'supervisorid' => $supervisorId,
            'title'        => $newPost->Title,
            'content'      => $newPost->Content,
            'imagesrc'     => $newPost->Image,
            'hiredate'     => $formatHire,
            'denied_cmt'   => $denied_cmt,
            'tags'         => $tags
        ]);
    }

    public function getImage(Uuid $uuid){
        $sql = "SELECT imagesrc FROM post WHERE uuid = :uuid";
        $img = parent::getRow($sql, ['uuid' => $uuid]);
        if (count($img) > 1) {
            return $img;
        } else {
            throw new Exception('This image not exist');
        }
    }

}
