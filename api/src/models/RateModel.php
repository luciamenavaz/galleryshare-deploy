<?php


class RateModel extends CoreModel
{
    public function __construct()
    {
        parent::__construct();
    }

    public function setRate($rate): int
    {
        $sql = "INSERT INTO to_rate (userid, postid, rate) 
                VALUE (:userid, :postid, :rate)";
        return parent::execQuery($sql, [
            'userid' => $rate->User->Uuid->__ToString(),
            'postid' => $rate->Post->Uuid->__ToString(),
            'rate' => $rate->Value
        ]);
    }

    public function getRates(Uuid $uuid): array
    {
        $query = 'SELECT rate FROM to_rate WHERE postid = :postid';
        $rates = parent::getArrayRows($query, ['postid' => $uuid->__ToString()]);
        $rateArray = [];
        foreach ($rates as $rate) {
            $rateArray[] = $rate;
        }
        return $rateArray;
    }

    public function rate(Uuid $uuidPost, Uuid $uuidUser): bool
    {
        $query = 'SELECT rate FROM to_rate WHERE postid = :postid AND userid = :userid';
        $rates = parent::execQuery($query, ['postid' => $uuidPost->__ToString(), 'userid' => $uuidUser]);

        if ($rates >= 1) {
            return false;
        } else {
            return true;
        }
    }

}
