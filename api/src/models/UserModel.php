<?php


class UserModel extends CoreModel
{
    public function __construct()
    {
        parent::__construct();
    }

    public function getLoginUser(string $idWord, string $pass)
    {
        $query = 'SELECT * FROM user WHERE (alias = :alias OR email = :alias) AND pass = :pass';
        $response = parent::getRow($query, [':alias' => $idWord, ':pass' => $pass]);
        if (count($response) > 0) {
            return $this->generateUser($response);
        }
        return false;
    }

    public function getAllUsers(): array
    {
        $query    = 'SELECT * FROM user';
        $response = parent::getArrayRows($query);

        $result = null;
        foreach ($response as $user) {
            $result[] = $this->generateUser($user);
        }

        return $result;
    }

    public function checkIfAliasExist(string $alias): array
    {
        $query = "SELECT * FROM user WHERE alias = :alias";
        $response = parent::getRow($query, [':alias' => $alias]);

        return $response;
    }

    public function checkIfEmailExist(string $email): array
    {
        $query = "SELECT * FROM user WHERE email = :email";
        $response = parent::getRow($query, [':email' => $email]);

        return $response;
    }

    public function insertUser(User $user): int
    {
        $birth = $user->BirthDate->format("Y-m-d H:i:s");
        $hire = $user->HireDate->format("Y-m-d H:i:s");
        $isadmin = intval($user->IsAdmin);
        $query = "INSERT INTO user (uuid, alias, name, email, pass, country, birthdate,  hiredate, isadmin)
                    VALUES ('$user->Uuid', :alias, :name, :email, :pass, :country, '$birth', '$hire', '$isadmin')";

        return parent::execQuery($query, array(
            "alias" => $user->UserAlias,
            "name" => $user->Name,
            "email" => $user->Email,
            "pass" => $user->Pass,
            "country" => $user->Country
        ));
    }

    public function deleteUserAccount(Uuid $userId): int
    {
        $this->deleteRates($userId);
        $this->deleteComments($userId);
        $this->changeSupervisorIdToPostsOf($userId);
        $this->deletePosts($userId);
        $this->deleteImages($userId);
        $this->deleteNotifications($userId);
        $response = $this->deleteUser($userId);
        return $response;
    }

    public function updateUserPass(Uuid $uuid, string $pass): int
    {
        $sql = "UPDATE user SET pass = :pass WHERE uuid = :uuid";
        return parent::execQuery($sql, array("uuid" => $uuid, "pass" => $pass));
    }

    public function updateUser(User $user): int
    {
        $birth = $user->BirthDate->format("Y-m-d H:i:s");
        $isadmin = intval($user->IsAdmin);
        $sql = "UPDATE user 
        SET alias = :alias, name = :name, email = :email, pass = :pass, country = :coun, birthdate = '$birth', isadmin = '$isadmin'
        WHERE uuid = :uuid";
        return parent::execQuery($sql, array(
            "uuid" => $user->Uuid,
            "alias" => $user->UserAlias,
            "name" => $user->Name,
            "email" => $user->Email,
            "pass" => $user->Pass,
            "coun" => $user->Country
        ));
    }

    public function checkIfOldPassMatch(Uuid $uuid, string $oldPass): array
    {
        $sql = "SELECT uuid, pass FROM user WHERE uuid = :uuid AND pass = :pass";
        $result = parent::getRow($sql, array("uuid" => $uuid, "pass" => $oldPass));

        return $result;
    }

    public function getUserRow(Uuid $uuid): array
    {
        $sql = "SELECT * FROM user WHERE uuid = :uuid";
        $response = parent::getRow($sql, array("uuid" => $uuid));

        return $response;
    }

    public function getUser(array $userRow): User
    {
        return $this->generateUser($userRow);
    }

    private function changeSupervisorIdToPostsOf(Uuid $userId)
    {
        $sql = "UPDATE post 
                SET supervisorid = (SELECT uuid
                                    FROM user 
                                    WHERE isadmin = 1 
                                    AND uuid != :uuid 
                                    LIMIT 1) 
                WHERE supervisorid = :uuid";

        parent::execQuery($sql, [":uuid" => $userId]);
    }

    private function deleteRates(Uuid $userId)
    {
        $sql = "DELETE FROM to_rate WHERE userid = :uuid";
        parent::execQuery($sql, [":uuid" => $userId]);
    }

    private function deleteComments(Uuid $userId)
    {
        $sql = "DELETE FROM comment WHERE userid = :uuid";
        parent::execQuery($sql, [":uuid" => $userId]);
    }

    private function deletePosts(Uuid $userId)
    {
        $sql = "SELECT * FROM post WHERE publisherid = :uuid";
        $posts = parent::getArrayRows($sql, [":uuid" => $userId]);

        foreach ($posts as $post) {
            $sql = "DELETE FROM comment WHERE postid = :id";
            parent::execQuery($sql, [":id" => $post["uuid"]]);

            $sql = "DELETE FROM to_rate WHERE postid = :id";
            parent::execQuery($sql, [":id" => $post["uuid"]]);
        }

        $sql = "DELETE FROM post WHERE publisherid = :uuid";
        parent::execQuery($sql, [":uuid" => $userId]);
    }

    private function deleteImages(Uuid $userId)
    {
        $sql = "DELETE FROM image WHERE userid = :uuid";
        parent::execQuery($sql, [":uuid" => $userId]);
    }

    private function deleteNotifications(Uuid $userId)
    {
        $sql = "DELETE FROM notification WHERE userid = :uuid";
        parent::execQuery($sql, [":uuid" => $userId]);
    }

    private function deleteUser(Uuid $userId): int
    {
        $sql = "DELETE FROM user WHERE uuid = :uuid";
        $response = parent::execQuery($sql, [":uuid" => $userId]);
        return $response;
    }

    private function generateUser(array $user): User
    {
        return new User(
            $user['alias'],
            $user['name'],
            $user['email'],
            $user['pass'],
            new Uuid($user['uuid']),
            $user['country'],
            new DateTimeImmutable($user['birthdate']),
            new DateTimeImmutable($user['hiredate']),
            $user['isadmin']
        );
    }
}
