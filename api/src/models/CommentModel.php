<?php


class CommentModel extends CoreModel
{
    public function insertComment($comment): int
    {
        $sql = "INSERT INTO comment (uuid, userid, postid, content, hiredate) 
                VALUE (:uuid, :userid, :postid, :content, :hiredate)";
        $formatHire = $comment->HireDate->format("Y-m-d H:i:s");
        return parent::execQuery($sql, [
            'uuid' => $comment->Uuid->__ToString(),
            'userid' => $comment->User->Uuid->__ToString(),
            'postid' => $comment->Post->__ToString(),
            'content' => $comment->Content,
            'hiredate' => $formatHire
        ]);
    }

    public function deleteComment(Uuid $uuid)
    {
        $sql = "DELETE FROM comment WHERE uuid = :uuid";
        return parent::execQuery($sql, [":uuid" => $uuid]);
    }

    public function getComments(Uuid $uuid){
        $sql = "SELECT * FROM comment WHERE postid = :uuid";
        $comments = parent::getArrayRows($sql, ['uuid' => $uuid]);
        $commentsArray = [];
        foreach ($comments as $comment){
            $commentsArray[] = $this->generateComment($comment);
        }
        return $commentsArray;
    }
    public function getComment(Uuid $uuid){
        $sql = "SELECT * FROM comment WHERE uuid = :uuid";
        $comments = parent::getRow($sql, ['uuid' => $uuid]);
        if (count($comments) > 1) {
            return $this->generateComment($comments);
        } else {
            throw new Exception('This comment not exist');
        }
    }

    public function generateComment($dataArray){
        $userModel = new UserModel();
        $publisher = $userModel->getUser($userModel->getUserRow(new Uuid($dataArray['userid'])));

        return new Comment(
            new Uuid($dataArray['uuid']),
            $publisher,
            new Uuid($dataArray['postid']),
            $dataArray['content'],
            new DateTimeImmutable($dataArray['hiredate'])
        );
    }
}
