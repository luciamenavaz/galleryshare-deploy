<?php
require_once(ENTITYPATH . 'Uuid.php');
require_once(ENTITYPATH . 'User.php');
require_once(MODELPATH . 'UserModel.php');
require_once(CTRLPATH . 'CoreController.php');
require_once(HELPERPATH . 'SessionHelper.php');

class Users extends CoreController
{

    public function loginUser()
    {
        $userModel = new UserModel();

        $dataArray = json_decode(file_get_contents("php://input"), true);

        $user = $userModel->getLoginUser($dataArray["idWord"], hash("sha256", $dataArray["pass"]));

        if (!$user) {
            $this->sendErrorMessage(400, 400, "User or pass incorrect");
        }

        SessionHelper::initUserSession($user);
        header('Content-Type: application/json');
        die(json_encode($user->getPublicData()));
    }

    public function logoutUser()
    {
        SessionHelper::closeUserSession();
    }

    public function createUser()
    {
        $user = json_decode(file_get_contents("php://input"), true);

        $newUser = new User(
            $user['alias'],
            $user['username'],
            $user['email'],
            hash("sha256", $user['pass']),
            new Uuid(),
            $user['country'],
            new DateTimeImmutable('@' . $user['birthDate']),
            new DateTimeImmutable(),
            $user['isAdmin']
        );

        $userModel  = new UserModel();
        $existAlias = count($userModel->checkIfAliasExist($newUser->UserAlias));
        $existEmail = count($userModel->checkIfEmailExist($newUser->Email));

        if ($existAlias > 0 && $existEmail > 0) {
            $this->sendErrorMessage(200, 4001, "User already exist");
        }

        if ($existAlias > 0) {
            $this->sendErrorMessage(200, 4011, "Alias already exist");
        }

        if ($existEmail > 0) {
            $this->sendErrorMessage(200, 4021, "Email already exist");
        }

        try {
            $userSession = SessionHelper::getUserSession();
        } catch (Exception $e) {
            $userSession = false;
        }


        if (
            $userSession && !$userSession->IsAdmin ||
            !$userSession && $newUser->IsAdmin
        ) {

            $this->sendErrorMessage(403, 4003, "Permission denied for this action");
        }


        try {
            $affectedRows = $userModel->insertUser($newUser);
        } catch (PDOException $e) {
            $this->sendErrorMessage(500, $e->getCode(), $e->getMessage());
        }

        if (!$userSession) {
            SessionHelper::initUserSession($newUser);
        }
        header('Content-Type: application/json');
        die(json_encode($newUser->getPublicData()));
    }

    public function deleteUser()
    {

        try {
            $userSession = SessionHelper::getUserSession();
        } catch (Exception $e) {
            $userSession = false;
        }

        if (!isset($_GET['uuid'])) {
            $this->sendErrorMessage(400, 4004, "Uuid doesn't exist");
        }

        $userId = $_GET['uuid'];

        if (!$userSession || ($userSession->Uuid != $userId && !$userSession->IsAdmin)) {
            $this->sendErrorMessage(403, 4003, "Permission denied for this action");
        }

        $userModel = new UserModel();


        try {
            $userId = new Uuid($userId);
            $affectedRows = $userModel->deleteUserAccount($userId);
        } catch (PDOException $e) {
            $this->sendErrorMessage(500, $e->getCode(), $e->getMessage());
        }

        if ($affectedRows <= 0) {
            $this->sendErrorMessage(200, 2001, "User not found");
        }

        $this->sendErrorMessage(201, 2002, "User removed correctly");
    }

    public function getAllUsers()
    {
        try {
            $userSession = SessionHelper::getUserSession();
        } catch (Exception $e) {
            $userSession = false;
        }

        if (!$userSession || !$userSession->IsAdmin) {
            $this->sendErrorMessage(403, 4003, "Permission denied for this action");
        }

        $userModel = new UserModel();
        $arrUsers  = $userModel->getAllUsers();
        $result    = null;
        foreach ($arrUsers as $user) {
            $result[] = $user->getPublicData();
        }
        header('Content-Type: application/json');
        echo json_encode($result);
    }

    public function updateUserPass()
    {
        try {
            $userSession = SessionHelper::getUserSession();
        } catch (Exception $e) {
            $userSession = false;
        }

        if (!$userSession) {
            $this->sendErrorMessage(403, 4003, "Permission denied for this action");
        }

        $passwords = json_decode(file_get_contents("php://input"), true);
        $userId = $userSession->Uuid;
        $userModel = new UserModel();
        $oldPassMatches = $userModel->checkIfOldPassMatch($userId, hash("sha256", $passwords["oldPass"]));

        if (count($oldPassMatches) <= 0) {
            $this->sendErrorMessage(400, 4005, "Old password doesn't match");
        }

        try {
            $affectedRows = $userModel->updateUserPass($userId, hash("sha256", $passwords["newPass"]));
        } catch (PDOException $e) {
            $this->sendErrorMessage(500, $e->getCode(), $e->getMessage());
        }

        $this->sendErrorMessage(201, 2002, "Password update correctly");
    }

    public function updateUser()
    {
        $newUserAttr = json_decode(file_get_contents("php://input"), true);

        try {
            $userSession = SessionHelper::getUserSession();
        } catch (Exception $e) {
            $userSession = false;
        }

        if (!$userSession) {
            $this->sendErrorMessage(403, 4003, "Permission denied for this action");
        }

        $userModel = new UserModel();

        if (!$userSession->IsAdmin && $newUserAttr["uuid"] != $userSession->Uuid) {
            $this->sendErrorMessage(403, 4003, "Permission denied for this action");
        }

        try {

            if (!$userSession->IsAdmin) {
                $userBD = $userModel->getUserRow($userSession->Uuid);

                if (empty($userBD)) {
                    $this->sendErrorMessage(200, 2001, "User not found");
                }

                $userBD = $userModel->getUser($userBD);
            }

            if ($userSession->IsAdmin) {
                $userBD = $userModel->getUserRow(new Uuid($newUserAttr["uuid"]));

                if (empty($userBD)) {
                    $this->sendErrorMessage(200, 2001, "User not found");
                }

                $userBD = $userModel->getUser($userBD);

                $userBD->setAdmin($newUserAttr["isAdmin"]);
                if (!empty($newUserAttr["pass"])) {
                    $userBD->setPass(hash("sha256", $newUserAttr["pass"]));
                }
            }

            if ($newUserAttr["alias"] != $userBD->UserAlias) {
                $existAlias = count($userModel->checkIfAliasExist($newUserAttr["alias"]));
            } else {
                $existAlias = -1;
            }

            if ($newUserAttr["email"] != $userBD->Email) {
                $existEmail = count($userModel->checkIfEmailExist($newUserAttr["email"]));
            } else {
                $existEmail = -1;
            }

            if ($existAlias > 0 && $existEmail > 0) {
                $this->sendErrorMessage(200, 4001, "User already exist");
            }

            if ($existAlias > 0) {
                $this->sendErrorMessage(200, 4011, "Alias already exist");
            }

            if ($existEmail > 0) {
                $this->sendErrorMessage(200, 4021, "Email already exist");
            }

            $userBD->setEmail($newUserAttr["email"]);
            $userBD->setUserAlias($newUserAttr["alias"]);
            $userBD->setName($newUserAttr["username"]);
            $userBD->setCountry($newUserAttr["country"]);
            $userBD->setBirthDate(new DateTimeImmutable('@' . $newUserAttr["birthDate"]));

            $affectedRows = $userModel->updateUser($userBD);
        } catch (Exception $e) {
            $this->sendErrorMessage(500, $e->getCode(), $e->getMessage());
        }

        if ($userSession->Uuid == $userBD->Uuid) {
            SessionHelper::initUserSession($userBD);
        }

        http_response_code(201);
        header('Content-Type: application/json');
        die(json_encode($userBD->getPublicData()));
    }
}
