<?php

require_once(ENTITYPATH . 'Uuid.php');
require_once(ENTITYPATH . 'Post.php');
require_once(ENTITYPATH . 'User.php');
require_once(ENTITYPATH . 'Rate.php');
require_once(ENTITYPATH . 'Comment.php');
require_once(MODELPATH . 'UserModel.php');
require_once(MODELPATH . 'PostModel.php');
require_once(MODELPATH . 'RateModel.php');
require_once(MODELPATH . 'CommentModel.php');
require_once(CTRLPATH . 'CoreController.php');
require_once(HELPERPATH . 'SessionHelper.php');

class Rates extends CoreController
{
    function setRate()
    {
        $dataArray = json_decode(file_get_contents("php://input"), true);
        //Comprobar que es un usuario logeado
        try {
            $sessionUser = SessionHelper::getUserSession();
        } catch (Exception $e) {
            $this->sendErrorMessage(403, 4003, 'Permission denied, no user logged');
        }
        /* Coger post y comprobar si existe o no */
        $getPost = new PostModel();
        try {
            $getPost = $getPost->getPost(new Uuid($dataArray['uuid']));
        } catch (Exception $e) {
            $this->sendErrorMessage(200, 2001, 'Post not found');
        }

        $rate = new Rate(
            $sessionUser,
            $getPost,
            $dataArray['rate_value']
        );

        $rateModel = new RateModel();
        try {
            $rateModel->setRate($rate);
            http_response_code(201);
            header('Content-Type: application/json');
            die(json_encode($rate->getPublicData()));

        } catch (Exception $e) {
            $this->sendErrorMessage(500, 2000, 'Error: the rate has not been created');
        }


    }
}
