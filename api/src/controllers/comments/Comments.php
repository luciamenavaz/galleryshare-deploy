<?php
require_once(ENTITYPATH . 'Uuid.php');
require_once(ENTITYPATH . 'Post.php');
require_once(ENTITYPATH . 'User.php');
require_once(ENTITYPATH . 'Comment.php');
require_once(ENTITYPATH . 'Rate.php');
require_once(MODELPATH . 'UserModel.php');
require_once(MODELPATH . 'PostModel.php');
require_once(MODELPATH . 'CommentModel.php');
require_once(MODELPATH . 'RateModel.php');
require_once(CTRLPATH . 'CoreController.php');
require_once(HELPERPATH . 'SessionHelper.php');

class Comments extends CoreController
{
    function createComment()
    {
        $dataArray = json_decode(file_get_contents("php://input"), true);

        //Comprobar que es un usuario logeado
        try {
            $sessionUser = SessionHelper::getUserSession();
        } catch (Exception $e) {
            $this->sendErrorMessage(403, 4003, 'Permission denied, no user logged');
        }
        /* Coger post y comprobar si existe o no */
        $getPost = new PostModel();
        try {
            $getPost = $getPost->getPost(new Uuid($dataArray['uuid']));
        } catch (Exception $e) {
            $this->sendErrorMessage(200, 2001, 'Post not found');
        }

        $comment = new Comment(
            new Uuid(),
            $sessionUser,
            $getPost->Uuid,
            $dataArray['content'],
            new DateTimeImmutable()
        );

        //Postmodel que llamará commentModel par actualizar el post
        $commentModel = new CommentModel();
        if ($commentModel->insertComment($comment)){
            http_response_code(201);
            header('Content-Type: application/json');
            die(json_encode($comment->getPublicData()));
        } else {
            $this->sendErrorMessage(500, 2000, 'Error: the comment has not been created');
        }


    }

    function deleteComment()
    {
        $uuid = $_GET['uuid'];
        //Comprobar que es un usuario logeado
        try {
            $sessionUser = SessionHelper::getUserSession();
        } catch (Exception $e) {
            $this->sendErrorMessage(403, 4003, 'Permission denied, no user logged');
        }

        $commentModel = new CommentModel();
        try {
            $getComment = $commentModel->getComment(new Uuid($uuid));
        } catch (Exception $e) {
            $this->sendErrorMessage(200, 2001, 'Comment not found');
        }
        $execRes = $commentModel->deleteComment(new Uuid($uuid));
        if ($execRes >= 1) {
            $this->sendErrorMessage(201, 2002, 'Comment removed correctly');
        }
    }
}
