<?php

use PHPMailer\PHPMailer\PHPMailer;
require_once ('vendor/PHPMailer/src/PHPMailer.php');
require_once ('vendor/PHPMailer/src/SMTP.php');
require_once ('vendor/PHPMailer/src/Exception.php');
require_once(CTRLPATH . 'CoreController.php');

class EmailController extends CoreController
{
    function sendEmail()
    {
        try {
            $request = json_decode(file_get_contents("php://input"), true);
            $mail = new PHPMailer();//indico a la clase que use SMTP
            $mail->IsSMTP();//permite modo debug para ver mensajes de las cosas que van ocurriendo
            //$mail->SMTPDebug  = 2;//Debo de hacer autenticación SMTP
            $mail->SMTPAuth   = true;
            $mail->SMTPSecure = "ssl";//indico el servidor de Gmail para SMTP
            $mail->Host       = "smtp.gmail.com";//indico el puerto que usa Gmail
            $mail->Port       = 465;//indico un usuario / clave de un usuario de gmail
            $mail->Username   = "info.galleryshare@gmail.com";
            $mail->Password   = "galleryshare21";
            $mail->setFrom('info.galleryshare@gmail.com', 'Admin Galleryshare');
            $mail->addReplyTo($request['email'], $request['firstname']);
            $mail->Subject = $request['subject'];
            $mail->MsgHTML($request['message']);//indico destinatario
            $mail->CharSet = 'UTF-8';
            $mail->AddAddress($mail->Username, "Admin Galleryshare");
            if (!$mail -> Send()) {
                echo 'false';
            } else {
                echo 'true';
            }
        } catch (\PHPMailer\PHPMailer\Exception $e) {
            echo $e;
        }
    }
}
