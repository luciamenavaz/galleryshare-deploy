<?php

require_once(ENTITYPATH . 'Comment.php');
require_once(ENTITYPATH . 'Uuid.php');
require_once(ENTITYPATH . 'Post.php');
require_once(ENTITYPATH . 'User.php');
require_once(ENTITYPATH . 'Rate.php');
require_once(MODELPATH . 'UserModel.php');
require_once(MODELPATH . 'PostModel.php');
require_once(MODELPATH . 'CommentModel.php');
require_once(MODELPATH . 'RateModel.php');
require_once(CTRLPATH . 'CoreController.php');
require_once(HELPERPATH . 'SessionHelper.php');

class Posts extends CoreController
{
    function createPost()
    {
        $dataArray = json_decode(file_get_contents("php://input"), true);

        //Comprobar que es un usuario logeado
        try {
            $sessionUser = SessionHelper::getUserSession();
        } catch (Exception $e) {
            $this->sendErrorMessage(403, 4003, 'Permission denied, no user logged');
        }

        if ($sessionUser->Uuid->__ToString() != $dataArray['publisher']['uuid'] && !$sessionUser->IsAdmin) {
            $this->sendErrorMessage(403, 4003, 'Permission denied for this action');
        }

        if ($sessionUser->IsAdmin) {
            $supervisor = $sessionUser;
        } else {
            $supervisor = null;
        }

        if ($sessionUser->Uuid->__ToString() == $dataArray['publisher']['uuid']) {
            $userPost = $sessionUser;
        } else {
            $userModel = new UserModel();
            $user      = $userModel->getUserRow(new Uuid($dataArray['publisher']['uuid']));
            $userPost  = new User(
                $user['alias'],
                $user['name'],
                $user['email'],
                $user['pass'],
                new Uuid($user['uuid']),
                $user['country'],
                new DateTimeImmutable($user['birthdate']),
                new DateTimeImmutable($user['hiredate']),
                $user['isadmin']
            );
            if (empty($userPost)) {
                $this->sendErrorMessage(403, 4003, 'The user has not been found');
            }
        }

        $post = new Post(new Uuid(),
            $dataArray['title'],
            $dataArray['content'],
            $userPost,
            $dataArray['image'],
            new DateTimeImmutable(),
            [],
            [],
            $dataArray['tags'],
            $supervisor
        );

        $postModel = new PostModel();
        if ($postModel->insertNewPost($post)) {
            $this->sendErrorMessage(201, 2000, 'The post has been created');
        } else {
            $this->sendErrorMessage(500, 2000, 'Error: the post has not been created');
        }

    }

    public function getAllPublicPosts()
    {
        $postModel     = new PostModel();
        $posts         = $postModel->getAllPost();
        $postsFormated = [];

        foreach ($posts as $post) {
            if ($post->isAproved()) {

                $postsFormated[] = [
                    'uuid'          => $post->Uuid->__ToString(),
                    'publisher'     => $post->Publisher->getPublicData(),
                    'title'         => $post->Title,
                    'contentResume' => $post->getResumeContent(),
                    'image'         => ImageHelper::reduceImage(500, $post->Image),
                    'hireDate'      => $post->HireDate->getTimestamp(),
                    'tags'          => $post->Tags,
                    'rate'          => $post->getRateAverage()
                ];

            }

        }

        header('Content-Type: application/json');
        die (json_encode($postsFormated));
    }

    public function getUserPosts()
    {
        try {
            $sessionUser = SessionHelper::getUserSession();
        } catch (Exception $e) {
            $this->sendErrorMessage(403, 4003, 'Permission denied, no user logged');
        }

        $postModel   = new PostModel();
        $responseArr = [];
        foreach ($postModel->getUserPosts($sessionUser->Uuid) as $post) {
            if ($post->Denied_cmt === null) {
                $denied_cmt = '';
            } else {
                $denied_cmt = $post->Denied_cmt;
            }
            $postData = [
                'uuid'       => $post->Uuid->__ToString(),
                'publisher'  => $post->Publisher->getPublicData(),
                'title'      => $post->Title,
                'hireDate'   => $post->HireDate->getTimestamp(),
                'denied_cmt' => $denied_cmt,
                'rate'       => $post->getRateAverage()
            ];

            if ($post->Supervisor != null) {
                $postData['supervisor'] = $post->Supervisor->getPublicData();
            } else {
                $postData['supervisor'] = null;
            }

            $responseArr[] = $postData;
        }

        header('Content-Type: application/json');
        die (json_encode($responseArr));
    }

    public function getAllUsersPosts()
    {
        try {
            $sessionUser = SessionHelper::getUserSession();
        } catch (Exception $e) {
            $this->sendErrorMessage(403, 4003, 'Permission denied, no user logged');
        }

        if (!$sessionUser->IsAdmin) {
            $this->sendErrorMessage(403, 4003, 'The user is not admin');
        }

        $postModel   = new PostModel();
        $responseArr = [];

        foreach ($postModel->getAllPost() as $post) {

            if ($post->Denied_cmt === null) {
                $denied_cmt = '';
            } else {
                $denied_cmt = $post->Denied_cmt;
            }

            $postData = [
                'uuid'       => $post->Uuid->__ToString(),
                'publisher'  => $post->Publisher->getPublicData(),
                'title'      => $post->Title,
                'content'    => strip_tags($post->Content),
                'hireDate'   => $post->HireDate->getTimestamp(),
                'denied_cmt' => $denied_cmt,
                'comments'   => $post->Comments,
                'rate'       => $post->getRateAverage()
            ];

            if ($post->Supervisor != null) {
                $postData['supervisor'] = $post->Supervisor->getPublicData();
            } else {
                $postData['supervisor'] = null;
            }

            $responseArr[] = $postData;
        }

        header('Content-Type: application/json');
        die (json_encode($responseArr));
    }

    public function delete()
    {
        try {
            $sessionUser = SessionHelper::getUserSession();
        } catch (Exception $e) {
            $this->sendErrorMessage(403, 4003, 'Permission denied, no user logged');
        }

        $postModel = new PostModel();
        try {
            $post = $postModel->getPost(new Uuid($_GET['uuid']));
        } catch (Exception $e) {
            $this->sendErrorMessage(200, 2001, 'Post not found');
        }

        if (!$sessionUser->IsAdmin && $sessionUser->Uuid != $post->Publisher->Uuid) {
            $this->sendErrorMessage(403, 4003, 'Permission denied, you don\'t have permissions');
        }

        $execRes = $postModel->removePost($post->Uuid);

        if ($execRes >= 1) {
            $this->sendErrorMessage(201, 2002, 'Post removed correctly');
        }
    }

    public function update()
    {
        $dataArray = json_decode(file_get_contents("php://input"), true);

        //Comprobar que es un usuario logeado
        try {
            $sessionUser = SessionHelper::getUserSession();
        } catch (Exception $e) {
            $this->sendErrorMessage(403, 4003, 'Permission denied, no user logged');
        }

        if ($sessionUser->Uuid->__ToString() != $dataArray['publisher']['uuid'] && !$sessionUser->IsAdmin) {
            $this->sendErrorMessage(403, 4003, 'Permission denied for this action');
        }

        if ($sessionUser->IsAdmin) {
            $supervisor = $sessionUser;
        } else {
            $supervisor = null;
        }

        if ($sessionUser->Uuid->__ToString() == $dataArray['publisher']['uuid']) {
            $userPost = $sessionUser;
        } else {
            $userModel = new UserModel();
            $user      = $userModel->getUserRow(new Uuid($dataArray['publisher']['uuid']));
            $userPost  = new User(
                $user['alias'],
                $user['name'],
                $user['email'],
                $user['pass'],
                new Uuid($user['uuid']),
                $user['country'],
                new DateTimeImmutable($user['birthdate']),
                new DateTimeImmutable($user['hiredate']),
                $user['isadmin']
            );
            if (empty($userPost)) {
                $this->sendErrorMessage(403, 4003, 'The user has not been found');
            }
        }

        if ($dataArray['tags'] == null) {
            $dataArray['tags'] = [];
        }
        $postModel = new PostModel();
        try {
            $oldPost = $postModel->getPost(new Uuid($dataArray['uuid']));
        } catch (Exception $e) {
            $this->sendErrorMessage(200, 2001, 'Post not found');
        }
        $oldPost->setTitle($dataArray['title']);
        $oldPost->setContent($dataArray['content']);
        $oldPost->setPublisher($userPost);
        $oldPost->setTags($dataArray['tags']);
        $oldPost->setSupervisor($supervisor);
        $oldPost->setDenied(null);
        if ($dataArray['image'] != null) {
            $imageSrc = ImageHelper::saveImage($dataArray['image']);

            $oldPost->setImage($imageSrc);
        }

        $postModel = new PostModel();
        if ($postModel->updatePost($oldPost)) {
            $this->sendErrorMessage(201, 2002, 'The post has been updated');
        } else {
            $this->sendErrorMessage(200, 2004, 'No changes applied');
        }
    }

    public function getPost()
    {
        $postId = $_GET['uuid'];

        if (!$postId) {
            $this->sendErrorMessage(200, 2001, 'Post Uuid no provided');
        }

        $postModel = new PostModel();
        try {
            $post = $postModel->getPost(new Uuid($postId));
        } catch (Exception $e) {
            echo $e;
            $this->sendErrorMessage(200, 2001, 'Post not found');
        }

        //COMPROBAR SI EL USUARIO QUE ESTÁ EN SESIÓN YA HA VOTADO PARA PONER ESTO EN FALSO O ES PÚBLICO
        $canRate = $this->canRate($post);
        $comments = $this->getCommentsData($post->Comments);

        $responsePost = [
            'uuid'      => $post->Uuid->__ToString(),
            'publisher' => $post->Publisher->getPublicData(),
            'title'     => $post->Title,
            'content'   => $post->Content,
            'image'     => ImageHelper::reduceImage(750, $post->Image),
            'hireDate'  => $post->HireDate->getTimestamp(),
            'tags'      => $post->Tags,
            'comments'  => $comments,
            'rate'      => $post->getRateAverage(),
            'canRate'   => $canRate
        ];
        http_response_code(201);
        header('Content-Type: application/json');

        echo json_encode($responsePost);
    }
    private function getCommentsData($comments){
        $commentArray = [];
        foreach ($comments as $comment){
            $commentArray[] = $comment->getPublicData();
        }

        return $commentArray;
    }

    public function approvePost()
    {
        try {
            $userLogged = SessionHelper::getUserSession();
        } catch (Exception $e) {
            $this->sendErrorMessage(403, 4003, 'Permission denied for this action');
        }

        if (!$userLogged->IsAdmin) {
            $this->sendErrorMessage(403, 4003, 'Permission denied for this action');
        }

        $postId = $_GET['uuid'];

        if (!$postId) {
            $this->sendErrorMessage(200, 2001, 'Post Uuid no provided');
        }

        $postModel = new PostModel();

        try {
            $post = $postModel->getPost(new Uuid($postId));
        } catch (Exception $e) {
            $this->sendErrorMessage(200, 2001, 'Post not found');
        }

        $post->setSupervisor($userLogged);

        if ($postModel->updatePost($post)) {
            $this->sendErrorMessage(201, 2002, 'The post has been approved');
        } else {
            $this->sendErrorMessage(200, 2001, 'Post not found');
        }

    }

    public function rejectPost()
    {
        try {
            $userLogged = SessionHelper::getUserSession();
        } catch (Exception $e) {
            $this->sendErrorMessage(403, 4003, 'Not user logged');
        }

        if (!$userLogged->IsAdmin) {
            $this->sendErrorMessage(403, 4003, 'Permission denied for this action');
        }

        $dataArray = json_decode(file_get_contents("php://input"), true);

        $postModel = new PostModel();

        try {
            $post = $postModel->getPost(new Uuid($dataArray['uuid']));
        } catch (Exception $e) {
            $this->sendErrorMessage(200, 2001, 'Post not found');
        }

        $post->setSupervisor($userLogged);
        $post->setDenied($dataArray['denied_cmt']);

        if ($postModel->updatePost($post)) {
            $this->sendErrorMessage(201, 2002, 'The post has been rejected');
        } else {
            $this->sendErrorMessage(200, 2001, 'Post not found');

        }
    }
    public function getAllImages()
    {
        $postModel = new PostModel();
        $data = $postModel->getAllPost();
        $allImages = [];

        foreach ($data as $value){
            if ($value->isAproved()) {
                $allImages[] = [
                    'uuid' => $value->Uuid->__ToString(),
                    'image' => ImageHelper::reduceImage(400, $value->Image)
                ];
            }
        }
        header('Content-Type: application/json');
        die (json_encode($allImages));
    }

    public function getImage()
    {
        $postId = $_GET['uuid'];
        $postModel = new PostModel();
        try{
            $data = $postModel->getImage(new Uuid($postId));
        }
        catch (Exception $e) {
            $this->sendErrorMessage(200, 2001, 'Image not found');
        }
        $img = ['image' => ImageHelper::reduceImage(750, $data['imagesrc'])];
        header('Content-Type: application/json');
        die (json_encode($img));
    }

    public function setPending(){
        $postId = $_GET['uuid'];

        try {
            $userLogged = SessionHelper::getUserSession();
        } catch (Exception $e) {
            $this->sendErrorMessage(403, 4003, 'Not user logged');
        }

        if (!$userLogged->IsAdmin) {
            $this->sendErrorMessage(403, 4003, 'Permission denied for this action');
        }

        $postModel = new PostModel();

        try {
            $post = $postModel->getPost(new Uuid($postId));
        } catch (Exception $e) {
            $this->sendErrorMessage(200, 2001, 'Post not found');
        }

        $post->setPending();

        if ($postModel->updatePost($post)) {
            $this->sendErrorMessage(201, 2002, 'The post has been set pedding');
        }

    }

    private function canRate($post){
        try {
            $sessionUser = SessionHelper::getUserSession();
            $rateModel = new RateModel();
            return $rateModel->rate(new Uuid($post->Uuid), new Uuid($sessionUser->Uuid));
        }
        catch (Exception $e){
            return false;
        }
    }

}
