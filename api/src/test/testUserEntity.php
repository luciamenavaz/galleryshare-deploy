<?php
require_once ('../entities/User.php');
require_once ('../entities/Uuid.php');

$userTest = new User(
    'Juani',
    'Juanito Del Amor',
    'juani@gmail.com',
    hash('sha256','MiPassword'),
    new Uuid(),
    'Spain',
    new DateTimeImmutable('@716860800'),
    new DateTimeImmutable(),
    false
);

