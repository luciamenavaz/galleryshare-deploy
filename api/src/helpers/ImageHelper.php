<?php


class ImageHelper
{
    public static function saveImage($base64)
    {
        $bin = base64_decode($base64);
        $img = imagecreatefromstring($bin);

        if (!$img) {
            throw new Exception('Base64 value is not a valid image');
        }

        $file = 'assets/images/' . uniqid() . '.png';
        imagepng($img, $file, 4);
        return $file;
    }

    public static function reduceImage($width, $route)
    {
        //Cramos el objeto imagen en función del tipo de archivo
        if (!file_exists($route)) {
            throw new InvalidArgumentException('File "'.$route.'" not found.');
        }
        switch ( strtolower( pathinfo( $route, PATHINFO_EXTENSION ))) {
            case 'jpeg':
            case 'jpg':
                $source = imagecreatefromjpeg($route);
                break;

            case 'png':
                $source = imagecreatefrompng($route);
                break;

            case 'gif':
                $source = imagecreatefromgif($route);
                break;

            default:
                throw new InvalidArgumentException('File "'.$route.'" is not valid jpg, png or gif image.');
        }

        //Obtenemos la nueva altura para mantener la relación de aspecto original
        $width_org = imagesx($source);
        $height_org = imagesy($source);
        $height_relative = $height_org/($width_org/$width);

        //Creamos el nuevo objeto imagen vacio para el resize
        $img_rsz = imagecreatetruecolor($width, $height_relative);

        //Copiamos el contenido a el nuevo objeto imagen
        imagecopyresampled($img_rsz, $source, 0, 0, 0, 0, $width, $height_relative, $width_org, $height_org);


        //Para poder convertir esta imagen en base64 tenemos que capturar la salida del script
        //para ello empleamos un buffer de salida.

        //Abrimos el buffer de salida
        ob_start();
        //Obtenemos el archivo imagen en formato PNG
        imagepng($img_rsz);
        //Capturamos la salida del buffer y la convertimos a base 64
        $dataImage = base64_encode(ob_get_contents());
        //Limpiamos el buffer y destruimos las imagenes
        ob_end_clean();
        imagedestroy($source);
        imagedestroy($img_rsz);

        //retornamos el binario de la imagen
        return($dataImage);
    }
}
