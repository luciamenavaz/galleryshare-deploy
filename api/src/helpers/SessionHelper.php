<?php

class SessionHelper
{
    public static function initUserSession(User $user): void
    {
        if (session_status() == PHP_SESSION_NONE) {
            session_start();
        }

        $_SESSION["user"] = serialize($user);
        session_write_close();
    }

    public static function getUserSession(): User
    {
        if (session_status() == PHP_SESSION_NONE) {
            session_start();
        }

        if (!isset($_SESSION['user'])) {
            throw new Exception('User session doesn\'t exists.');
        }
        $user = unserialize($_SESSION['user']);
        session_write_close();
        return $user;
    }

    public static function closeUserSession(): void
    {
        if (session_status() == PHP_SESSION_NONE) {
            session_start();
        }
        if (ini_get("session.use_cookies")) {
            $params = session_get_cookie_params();
            setcookie(
                session_name(),
                '',
                time() - 42000,
                $params["path"],
                $params["domain"],
                $params["secure"],
                $params["httponly"]
            );
        }
        session_destroy();
    }
}
