<?php

/**
 * LOAD CORE CONSTANTS
 */
require_once('./system/config.php');


/**
 * LOAD ROUTER ARRAY
 */
include_once('./system/router.php');

/**
 * LOAD CORE CLASSES
 */
require_once('./system/URIParser.php');
require_once('./system/CoreModel.php');

/**
 * THIS SCRIPT VALUE IF THE URI IS CORRECT AND EXTRACT DE REQUIRED PATH
 * FOR SEND TO THE PARSER
 */

$urlFull = $_SERVER['PHP_SELF'];

$pos = strpos($urlFull, 'index.php');
if ($pos === false) {
    http_response_code(404);
    die('Not found');
}

$requiredPath = explode('index.php', $urlFull)[1];

/** @var array $routes */
$parser = new URIParser($routes, $requiredPath, $_SERVER['REQUEST_METHOD']);
